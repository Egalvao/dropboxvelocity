package com.dropbox.android.sample;

public class MySpeed {
	
	public static int UTIL_KB = 1024;
	public static int UTIL_MB = UTIL_KB * 1024;
	public static int UTIL_mili = 1000;
	public static int UTIL_nano = 1000000000;
	
	private double calculateSpeed(double size, double timeInSec){
		double velocity = size/timeInSec ;
		return velocity;
	}

	//Aqui passamos os argumentos diretamente para obter resultado na Medida Desejada.
	
	
	public double calculateSpeedInKBps(double sizeInKByte, double timeInSec){
		double velocity = calculateSpeed(sizeInKByte, timeInSec);
		return velocity;
	}
	public double calculateSpeedInBytePerSecond(double sizeInByte, double timeInSec){
		double velocity = calculateSpeed(sizeInByte, timeInSec);
		return velocity;
	}

	public double calculateSpeedInMBps(double sizeInMByte, double timeInSec){
		double velocity = calculateSpeed(sizeInMByte, timeInSec);
		return velocity;
	}
	
	//Conversões de Unidade de tempo

	public double converterSec2Ms(double timeInSec){
		double milisegundo = timeInSec*UTIL_mili;
		return milisegundo;
	}

	public double converterSec2Ns(double timeInSec){
		double nanosegundo = timeInSec * UTIL_nano;
		return nanosegundo;
	}

	public double converterMs2Sec(double timeInMili){
		double segundo = timeInMili * ((double)1/UTIL_mili);
		return segundo;
	}
	
	public double converterNs2Sec(double timeInNano){
		double segundo = timeInNano * ((double)1/UTIL_nano);
		return segundo;
	}
	
	//Converter Unidade de Tamanho do arquivo.
	
	public double converterByte2Kbyte(double sizeInByte){
		double size = ((double)1/UTIL_KB)*sizeInByte;
		return size;
	}
	
	public double converterByte2Mbyte(double sizeInByte){
		double size = ((double)1/UTIL_MB)*sizeInByte;
		return size;
	}
	
	public double converterKByte2byte(double sizeInKByte){
		return (double)(sizeInKByte*UTIL_KB);
	}
	
	public double converterKByte2Mbyte(double sizeInKByte){
		return (sizeInKByte * (double)(1/UTIL_KB));
	}
	
	public double converterMByte2byte(double sizeInMByte){
		return (sizeInMByte*UTIL_MB);
	}
	
	public double converterMByte2Kbyte(double sizeInMByte){
		return sizeInMByte * UTIL_KB;
	}
	
	
	
}

